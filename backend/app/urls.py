"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.views.static import serve
from django.conf.urls.static import static
from core.page.views import PageView
from django.conf.urls.i18n import i18n_patterns
from multiurl import multiurl
from django.http import Http404
from multiurl import ContinueResolving
from filebrowser.sites import site as filebrowser_site

from patriot.views import NewsView, HistoryView, form_catcher, EventsView, OrgsView, add_like, add_dislike, SocialView


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^admin/filebrowser/', include(filebrowser_site.urls)),
]

urlpatterns += [
    url(r'^api/formcatcher/$', form_catcher, name='form_catcher'),
    url(r'^api/add_like/$', add_like, name='add_like'),
    url(r'^api/add_dislike/$', add_dislike, name='add_dislike'),
    url('', include('social_django.urls', namespace='social'))
]

urlpatterns += i18n_patterns(
    url(r'^$', PageView.as_view()),
    multiurl(
        url(r'^$', PageView.as_view()),
        url(r'^(?P<url>.*?)/$', PageView.as_view()),
        url(r'^(?P<url>.*?)/$', NewsView.as_view()),
        url(r'^(?P<url>.*?)/$', HistoryView.as_view()),
        url(r'^(?P<url>.*?)/$', EventsView.as_view()),
        url(r'^(?P<url>.*?)/$', OrgsView.as_view()),
        url(r'^(?P<url>.*?)/$', SocialView.as_view()),



        catch=(Http404, ContinueResolving),
    ),
    prefix_default_language=False,
)



urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += [
    url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
]

from core.options.models import SiteConfiguration
from django.contrib import admin
from solo.admin import SingletonModelAdmin
from modeltranslation.admin import TabbedTranslationAdmin

# admin.site.register(SiteConfiguration, SingletonModelAdmin)

@admin.register(SiteConfiguration)
class SiteConfigurationAdmin(SingletonModelAdmin):
    save_on_top = True
    menu_title = "Конфигурация сайта"
    menu_group = "Настройки"

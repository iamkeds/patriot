from modeltranslation.translator import register, TranslationOptions
from core.options.models import SiteConfiguration


@register(SiteConfiguration)
class SiteConfigurationTranslationOptions(TranslationOptions):
    fields = ('site_name', 'site_name_long', 'seo_keywords', 'seo_description', 'address')

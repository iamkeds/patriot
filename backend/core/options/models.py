from django.db import models
from solo.models import SingletonModel
from filebrowser.fields import FileBrowseField


class SiteConfiguration(SingletonModel):
    site_name = models.CharField(max_length=255, default='Название сайта', verbose_name='Название сайта')
    site_name_long = models.CharField(max_length=255, default='Название сайта длинное (в шапке)', verbose_name='Название сайта длинное (в шапке)')
    seo_keywords = models.CharField(max_length=255, default='SEO keywords по умолчанию', verbose_name='SEO keywords по умолчанию')
    seo_description = models.CharField(max_length=255, default='SEO description по умолчанию', verbose_name='SEO description по умолчанию')
    og_locale = models.CharField(max_length=255, null=True, blank=True, default='ru_RU', verbose_name='og locale')
    article_author = models.CharField(max_length=255, null=True, blank=True, verbose_name='Автор статей')
    email_admin = models.CharField(max_length=255, null=True, blank=True, verbose_name='E-mail администратора')
    scripts = models.TextField(null=True, blank=True, verbose_name="Блок скриптов")
    robots_text = models.TextField(null=True, blank=True, verbose_name="Содержимое robots.txt")
    vk_link = models.CharField(max_length=255, default='Ссылка на группу ВКонтакте', verbose_name='Ссылка на группу ВКонтакте')
    fb_link = models.CharField(max_length=255, default='Ссылка на группу Facebook', verbose_name='Ссылка на группу Facebook')
    phone = models.CharField(max_length=30, default='Номер телефона', verbose_name='Номер телефона')
    address = models.CharField(max_length=255, default="Адрес", verbose_name="Адрес")
    logo = FileBrowseField("Логотип", max_length=500, extensions=[".png", ".gif"], blank=True, null=True)

    def __str__(self):
        return 'Site Configuration'

    class Meta:
        verbose_name = "Конфигурация сайта"

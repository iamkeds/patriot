from modeltranslation.translator import register, TranslationOptions
from core.page.models import Page, IncludeArea


@register(Page)
class PageTranslationOptions(TranslationOptions):
    fields = ('title', 'content', 'seo_title', 'seo_keywords', 'seo_description', 'seo_author')


@register(IncludeArea)
class IncludeAreaTranslationOptions(TranslationOptions):
    fields = ('content',)

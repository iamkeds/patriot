from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView, DetailView
from core.page.models import Page, IncludeArea
from django.conf import settings
from django.http import Http404


def home_page_context(request):
    return {
        'is_home_page': True
    }


def default_page_context(request):
    return {}


class PageView(DetailView):
    def get_template_names(self):
        """
        Метод для получения темплейта для страницы. Структуру темплейтов можно посмотреть в модели page.models.Page.
        """
        return [self.object.get_template()]

    def get_context_data(self, **kwargs):
        """
        Метод для добавления контекста к нашей странице.
        """
        context = super(PageView, self).get_context_data(**kwargs)
        local_includes = dict(IncludeArea.objects.filter(page=self.object).values_list('code', 'content'))
        context.update(local_includes)
        context.update(self.object.get_context(self.request))
        return context

    def get_object(self, queryset=None):
        """
        Метод для получения объекта страницы. Сравнивает текущий урл со slug, которые мы получаем из родительских страниц нашей страницы.
        """
        if 'url' in self.kwargs:
            slugs = self.kwargs['url'].split('/')
            parent = None
            for slug in slugs:
                pages = Page.objects.filter(slug=slug, parent=parent)
                parent = pages.first()
            page = pages.first()
            if page is None:
                raise Http404
            return page
        page = Page.objects.filter(page_type=0).first()  # magic number "home page"
        if page is None:
            raise Http404
        return page

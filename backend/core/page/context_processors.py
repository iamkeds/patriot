
def include_area_processor(request):
    from core.page.models import IncludeArea
    includes = dict(IncludeArea.objects.filter(page=None).values_list('code', 'content'))
    return includes

from django.contrib import admin
from core.page.models import Page, IncludeArea
from mptt.admin import DraggableMPTTAdmin
from modeltranslation.admin import TranslationTabularInline
from modeltranslation.admin import TabbedTranslationAdmin


class IncludeAreaInline(TranslationTabularInline):
    model = IncludeArea


@admin.register(Page)
class PageAdmin(DraggableMPTTAdmin, TabbedTranslationAdmin):
    list_display = (
        'tree_actions',
        'indented_title',
        'page_type',
        'is_active',
        'slug',
        'get_absolute_url',
    )
    list_display_links=(
        'indented_title',
    )
    readonly_fields=(
        'get_absolute_url',
    )
    save_on_top=True
    prepopulated_fields={'slug': ('title',)}
    menu_title="Страницы"
    menu_group="Структура"
    inlines=[
        IncludeAreaInline,
    ]
    list_filter = ('is_active', 'created_at',)
    search_fields = ('title', 'content')


class IncludeAreaOnlyNoneAdmin(TabbedTranslationAdmin):
    save_on_top = True
    menu_title = "Общие включаемые области"
    menu_group = "Настройки"
    list_display = ('title', 'code', 'content')
    search_fields = ('title', 'content')

    def get_queryset(self, request):
        qs = super(IncludeAreaOnlyNoneAdmin, self).get_queryset(request)
        return qs.filter(page=None)

admin.site.register(IncludeArea, IncludeAreaOnlyNoneAdmin)

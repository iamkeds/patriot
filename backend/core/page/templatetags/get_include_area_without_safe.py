from django import template
from core.page.models import IncludeArea

from django.utils.html import format_html,escape,html_safe

register = template.Library()


@register.simple_tag
def get_include_area_without_safe(request, code, *args, **kwargs):
    try:
        if 'page' in kwargs:
            include_area = IncludeArea.objects.get(code=code, page=kwargs.get('page', None))
        else:
            include_area = IncludeArea.objects.get(code=code)
    except IncludeArea.DoesNotExist:
        include_area = IncludeArea.objects.create(code=code, **kwargs)

    # print(include_area.content)
    return include_area.content

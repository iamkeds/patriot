from django import template
from core.page.models import IncludeArea
from django.utils.safestring import mark_safe



register = template.Library()


@register.simple_tag
def get_include_area(request, code, *args, **kwargs):
    try:
        if 'page' in kwargs:
            include_area = IncludeArea.objects.get(code=code, page=kwargs.get('page', None))
        else:
            include_area = IncludeArea.objects.get(code=code)
    except IncludeArea.DoesNotExist:
        include_area = IncludeArea.objects.create(code=code, **kwargs)
    return mark_safe(include_area.content)

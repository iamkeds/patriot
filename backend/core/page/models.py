from django.db import models
from core.base_page.models import BasePage
from mptt.models import MPTTModel, TreeForeignKey
from django.utils import translation
from core.helper.helpers import get_paginated_objects
from filebrowser.fields import FileBrowseField
from ckeditor_uploader.fields import RichTextUploadingField
from django.conf import settings
from django.utils.module_loading import import_string


CHOICES_PAGE_TYPES = []
for k,v in settings.PAGE_TYPES.items():
    CHOICES_PAGE_TYPES.append((k, v['title']))
    

class Page(BasePage, MPTTModel):
    """
    Страница.
    """
    page_type = models.IntegerField(default=1, choices=CHOICES_PAGE_TYPES, verbose_name='Тип страницы')
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True, verbose_name='Родительская страница')

    def __str__(self):
        return "{} (Тип: {})".format(self.title, CHOICES_PAGE_TYPES[self.page_type][1])

    class Meta:
        verbose_name = 'Страница'
        verbose_name_plural = 'Страницы'
        ordering = ('-created_at',)

    def get_absolute_url(self):
        current_language_code_url_prefix = translation.get_language()
        if current_language_code_url_prefix == settings.LANGUAGE_CODE:
            current_language_code_url_prefix = ''
        else:
            current_language_code_url_prefix = '/' + current_language_code_url_prefix
        if self.slug and self.page_type != 0:
            obj = self
            url_arr = [self.slug]
            while obj.parent is not None:
                obj = obj.parent
                url_arr.insert(0, obj.slug)
            return "{}/{}/".format(current_language_code_url_prefix, '/'.join(url_arr))
        return "{}/".format(current_language_code_url_prefix)
    get_absolute_url.short_description = 'URL'

    def get_template(self):
        return settings.PAGE_TYPES[self.page_type]['template']

    def get_context(self, request=None):
        if 'context' in settings.PAGE_TYPES[self.page_type]:
            try:
                context_function = import_string(settings.PAGE_TYPES[self.page_type]['context'])
            except:
                context_function = None
            if context_function is not None:
                return context_function(request)
        return {}


class IncludeArea(models.Model):
    """
    Дает возможность изменять контент в дополнительных блоках
    """
    class Meta:
        verbose_name = 'Включаемая область'
        verbose_name_plural = 'Включаемые области'
        ordering = ('title',)

    title = models.CharField(max_length=100, verbose_name='Название', help_text='Отображается только для администратора')
    code = models.CharField(max_length=100, default='none', verbose_name='Код')
    page = models.ForeignKey(Page, verbose_name='На каких страницах отображать?', blank=True, null=True)
    # content = models.TextField(verbose_name='Содержимое')
    content = RichTextUploadingField(verbose_name='Содержимое', blank=True, default='', config_name='awesome_ckeditor')

    def __str__(self):
        return self.title


from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField


class BasePage(models.Model):
    """
    Базовая страница, на основе которой создаются все прочие страницы.
    """
    title = models.CharField(max_length=100, verbose_name='Название')
    content = RichTextUploadingField(verbose_name='Содержимое', blank=True, default='', config_name='awesome_ckeditor')
    is_active = models.BooleanField(default=True, verbose_name='Включено')
    slug = models.SlugField(max_length=150, verbose_name='ЧПУ', blank=True, default='')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата изменения')
    seo_title = models.CharField(max_length=250, verbose_name='SEO заголовок страницы (title)', blank=True, default='')
    seo_keywords = models.CharField(max_length=250, verbose_name='SEO ключевые слова (keywords)', blank=True, default='')
    seo_description = models.CharField(max_length=250, verbose_name='SEO описание (description)', blank=True, default='')
    seo_author = models.CharField(max_length=250, verbose_name='SEO автор (author)', blank=True, default='')
    seo_og_type = models.CharField(max_length=250, verbose_name='SEO og:type', blank=True, default="website")
    
    # managers
    objects = models.Manager()

    class Meta:
        verbose_name = 'Базовая страница'
        verbose_name_plural = 'Базовые страницы'
        ordering = ('title',)
        abstract = True

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        if self.slug:
            return "/{}/".format(self.slug)
        return "/"
    get_absolute_url.short_description = 'URL'

    def get_verbose_model_name(self):
        return self._meta.verbose_name
    get_verbose_model_name.short_description = 'Название модели'

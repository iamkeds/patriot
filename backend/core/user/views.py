from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.views import LoginView
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import TemplateView
from django.contrib.auth import (
    REDIRECT_FIELD_NAME, get_user_model, login as auth_login,
    logout as auth_logout, update_session_auth_hash,
    _get_backends)

from user.models import User
from .forms import AuthForm


class ProfileView(TemplateView):
    template_name = "registration/user_detail.html"


class AuthView(LoginView):
    form_class = AuthForm

    def is_registration(self):
        return 'reg' in self.request.GET

    def get_form_kwargs(self):
        default_kwargs = super().get_form_kwargs()
        default_kwargs['is_reg'] = self.is_registration()
        return default_kwargs

    def form_valid(self, form):
        if not self.is_registration():
            return super().form_valid(form)

        user = User(
            username=form.cleaned_data['username']
        )
        user.set_password(form.cleaned_data['password'])
        user.save()
        # грязный хак
        # _get_backends(return_tuples=True)
        auth_login(self.request, user, backend='django.contrib.auth.backends.ModelBackend')
        return HttpResponseRedirect(self.get_success_url())

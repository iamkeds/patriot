from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django import forms
from user.models import User


class AuthForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        is_reg = kwargs.pop('is_reg', False)
        super().__init__(*args, **kwargs)
        self.is_reg = is_reg

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        if not self.is_reg:

            if username is not None and password:
                self.user_cache = authenticate(self.request, username=username, password=password)
                if self.user_cache is None:
                    raise forms.ValidationError(
                        self.error_messages['invalid_login'],
                        code='invalid_login',
                        params={'username': self.username_field.verbose_name},
                    )
                else:
                    self.confirm_login_allowed(self.user_cache)
        else:
            if User.objects.filter(username=username).exists():
                raise forms.ValidationError(
                    'Пользователь с таким именем уже существует'
                )

        return self.cleaned_data

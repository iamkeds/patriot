from django.utils import translation


def current_language_processor(request):
    current_language_code = translation.get_language()
    return {
        'current_language_code': current_language_code
    }

from django.utils.deprecation import MiddlewareMixin


class LastSessionMiddleware(MiddlewareMixin):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        from core.user.models import User
        from django.utils import timezone
        if request.user.is_authenticated():
            try:
                user = User.objects.get(id=request.user.id)
                user.last_session_at = timezone.now()
                user.save()
            except:
                pass
        return response

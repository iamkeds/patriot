from django.db import models
# from core.helper.models import LegalAddressModelMixin, FactAddressModelMixin
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from core.helper.helpers import get_file_path
from django.utils import timezone
from filebrowser.fields import FileBrowseField

CHOICES_SEX = (
    (0, 'Женский'),
    (1, 'Мужской'),
    (2, 'Не указан'),
)


class User(AbstractUser):  # , LegalAddressModelMixin, FactAddressModelMixin
    sex = models.IntegerField(default=2, choices=CHOICES_SEX, verbose_name='Пол')
    patronymic_name = models.CharField(max_length=100, verbose_name='Отчество', blank=True, null=True)
    phone = models.CharField(max_length=100, verbose_name='Телефон', blank=True, default='')
    avatar = FileBrowseField("Аватар", max_length=500, extensions=[".jpg", ".png", ".gif"], blank=True, null=True)
    birthday = models.DateField(blank=True, null=True, verbose_name='Дата рождения')
    last_session_at = models.DateTimeField(default=timezone.now, verbose_name="Дата последнего посещения сайта")

    def __str__(self):
        name = self.username
        if self.first_name and self.last_name:
            name = '{} {}'.format(self.first_name, self.last_name)
        return name

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'
        ordering = ['first_name', 'last_name']
        abstract = False

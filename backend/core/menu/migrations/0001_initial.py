# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-28 13:49
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('page', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MenuItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100, verbose_name='Название')),
                ('title_ru', models.CharField(max_length=100, null=True, verbose_name='Название')),
                ('menu_type', models.CharField(choices=[('top_menu', 'Верхнее меню')], default='top_menu', max_length=100, verbose_name='Тип меню')),
                ('url', models.CharField(blank=True, max_length=1000, null=True, verbose_name='Внешний URL')),
                ('is_active', models.BooleanField(default=True, verbose_name='Включено')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Дата изменения')),
                ('sort', models.IntegerField(default=100, help_text='Чем меньше число, тем выше будет элемент в списке.', verbose_name='Сортировка')),
                ('page', models.ForeignKey(blank=True, help_text='Если этот пункт не выбран, то будет использовано следующее поле "Внешний URL"', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='page', to='page.Page', verbose_name='Страница, на которую ведет пункт меню')),
                ('show_on_pages', models.ManyToManyField(blank=True, help_text='Когда ничего не выбрано, отображается на всех.', related_name='show_on_pages', to='page.Page', verbose_name='На каких страницах отображать')),
            ],
            options={
                'verbose_name': 'Пункт меню',
                'verbose_name_plural': 'Пункты меню',
                'ordering': ('sort', 'title'),
            },
        ),
    ]

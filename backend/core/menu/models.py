from django.db import models
from core.page.models import Page
from django.utils import translation
from django.conf import settings


class MenuItem(models.Model):
    """
    Пункты меню.
    """
    title = models.CharField(max_length=100, verbose_name='Название')
    menu_type = models.CharField(default='top_menu', max_length=100, choices=settings.CHOICE_MENU_TYPES, verbose_name='Тип меню')
    page = models.ForeignKey(Page, null=True, blank=True, verbose_name='Страница, на которую ведет пункт меню', help_text='Если этот пункт не выбран, то будет использовано следующее поле "Внешний URL"', related_name='page')
    url = models.CharField(max_length=1000, null=True, blank=True, verbose_name='Внешний URL')
    is_active = models.BooleanField(default=True, verbose_name='Включено')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата изменения')
    show_on_pages = models.ManyToManyField(Page, blank=True, verbose_name='На каких страницах отображать', help_text='Когда ничего не выбрано, отображается на всех.', related_name='show_on_pages')
    sort = models.IntegerField(default=100, verbose_name='Сортировка', help_text='Чем меньше число, тем выше будет элемент в списке.')
    # virtual fields
    hidden_menu_item_class = ''
    active_menu_item_class = ''
    # managers
    objects = models.Manager()

    class Meta:
        verbose_name = 'Пункт меню'
        verbose_name_plural = 'Пункты меню'
        ordering = ('sort', 'title',)

    def __str__(self):
        return self.title

    def get_link(self):
        if self.page:
            return self.page.get_absolute_url()
        elif self.url:
            if self.url.startswith('/'):
                current_language_code_url_prefix = translation.get_language()
                if current_language_code_url_prefix == settings.LANGUAGE_CODE:
                    return "{}".format(self.url)
                return "/{}{}".format(current_language_code_url_prefix, self.url)
            else:
                return self.url
        else:
            return '#'

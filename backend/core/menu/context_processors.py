from core.menu.models import MenuItem
from django.db.models import Q
from core.page.models import Page
from django.conf import settings


def menu_processor(request):
    # helper
    current_path = request.get_full_path()
    if current_path:
        slugs = current_path.split('/')[1:-1]
        parent = None
        if len(slugs) == 0:  # home page
            page = Page.objects.filter(page_type=0)[:1]
        else:
            for slug in slugs:
                pages = Page.objects.filter(slug=slug, parent=parent)
                parent = pages.first()
            page = pages.first()
        current_page = page
    else:
        current_page = Page.objects.filter(page_type=0).first()  # magic number "home page"
    #
    context = {
        'menus': {}
    }
    for menu_type_arr in settings.CHOICE_MENU_TYPES:
        context['menus'][menu_type_arr[0]] = MenuItem.objects.filter(is_active=True, menu_type=menu_type_arr[0])
        for menu_item in context['menus'][menu_type_arr[0]]:
            if (menu_item.show_on_pages.all().count() > 0) and (current_page not in menu_item.show_on_pages.all()):
                menu_item.hidden_menu_item_class = 'dn'
            if menu_item.get_link() == current_path:
                menu_item.active_menu_item_class = 'active'
            elif current_path.startswith(menu_item.get_link()):
                menu_item.active_menu_item_class = 'active'
            elif menu_item.url and menu_item.url.endswith(current_path):
                menu_item.active_menu_item_class = 'active'
    return context

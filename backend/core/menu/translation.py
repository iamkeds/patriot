from modeltranslation.translator import register, TranslationOptions
from core.menu.models import MenuItem


@register(MenuItem)
class MenuItemTranslationOptions(TranslationOptions):
    fields = ('title',)

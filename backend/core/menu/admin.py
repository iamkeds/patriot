from django.contrib import admin
from core.menu.models import MenuItem
from django.forms import CheckboxSelectMultiple
from django.db import models
from modeltranslation.admin import TabbedTranslationAdmin


class MenuItemAdmin(TabbedTranslationAdmin):
    save_on_top = True
    list_display = ('title', 'menu_type', 'page', 'url', 'is_active', 'sort')
    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }
    list_filter = ('menu_type', 'is_active')
    menu_title = "Меню"
    menu_group = "Структура"
    search_fields = ('title',)

admin.site.register(MenuItem, MenuItemAdmin)

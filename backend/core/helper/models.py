from django.db import models
from core.helper.helpers import get_file_path


class FactAddressModelMixin(models.Model):
    """
    Миксин - фактический адрес.
    """
    country = models.CharField(max_length=100, verbose_name='Страна')
    zip_code = models.IntegerField(default=0, verbose_name='Почтовый индекс')
    region = models.CharField(max_length=100, verbose_name='Область')
    district = models.CharField(max_length=100, verbose_name='Район')
    city = models.CharField(max_length=100, verbose_name='Город')
    street = models.CharField(max_length=100, verbose_name='Улица')
    house = models.CharField(max_length=100, verbose_name='Дом')
    building = models.CharField(max_length=100, verbose_name='Корпус/строение')
    apartment = models.CharField(max_length=100, verbose_name='Квартира/офис')

    class Meta:
        abstract = True


class LegalAddressModelMixin(models.Model):
    """
    Миксин - легальный (юридический) адрес.
    """
    legal_country = models.CharField(max_length=100, verbose_name='Страна')
    legal_zip_code = models.IntegerField(default=0, verbose_name='Почтовый индекс')
    legal_region = models.CharField(max_length=100, verbose_name='Область')
    legal_district = models.CharField(max_length=100, verbose_name='Район')
    legal_city = models.CharField(max_length=100, verbose_name='Город')
    legal_street = models.CharField(max_length=100, verbose_name='Улица')
    legal_house = models.CharField(max_length=100, verbose_name='Дом')
    legal_building = models.CharField(max_length=100, verbose_name='Корпус/строение')
    legal_apartment = models.CharField(max_length=100, verbose_name='Квартира/офис')

    class Meta:
        abstract = True

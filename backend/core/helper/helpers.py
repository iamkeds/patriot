import uuid
import uuslug
import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
import random
import string


def get_paginated_objects(request, objects_qs):
    page = request.GET.get('page', 1)
    paginator = Paginator(objects_qs, settings.PAGINATION_ELEMENTS_PER_PAGE)
    try:
        objects = paginator.page(page)
    except PageNotAnInteger:
        objects = paginator.page(1)
    except EmptyPage:
        objects = paginator.page(paginator.num_pages)
    return objects


def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    today = datetime.date.today()
    filename = "%s.%s" % (uuslug.slugify(".".join(filename.split('.')[:-1])), ext)
    return 'uploads/%s/%s/%s' % (today.year, today.month, filename)


def get_random_string(char_count=8):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=char_count))

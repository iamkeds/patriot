from django import forms
from django.forms import ModelForm
from patriot.models import Events


class AddEventForm(ModelForm):
    class Meta:
        model = Events
        fields = ['title', 'content', 'short_descr', 'date_at', 'contacts']


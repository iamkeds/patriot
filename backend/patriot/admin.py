from django.contrib import admin
from django.forms import TextInput, Textarea
from django.db import models
from patriot.models import News, History, PropertyImage, Events, PropertyImageEvents, PropertyImageEventsOrg, Orgs, PropertyImageOrgs, PropertyImageOrgsOrg, SocialInfo, Comments, Photo_Video


# @admin.register(VisitorItem)
# class VisitorItemAdmin(admin.ModelAdmin):
#     save_on_top = True
#     search_fields = ('email', 'phone', 'title_message', 'title_form')
#     list_display = ('id', 'email', 'phone', 'title_message', 'title_form')
#     fields = ('title_message', ('email', 'phone'), 'title_form', 'text_form_1', 'text_form_2')
#     list_filter = ('title_form',)


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    # save_on_top = True
    # list_display = ('title', 'content', 'image', 'is_active', 'created_at', 'slug', 'seo_title', 'seo_keywords', 'seo_description')
    fields = ('title', 'content', 'image', 'is_active', 'date_at', 'slug', 'seo_title', 'seo_keywords', 'seo_description')
    prepopulated_fields = {'slug': ('title',)}
    menu_title = "Новости"
    menu_group = "Новости"
    # readonly_fields = ('created_at',)
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '134'})},
    }


class PropertyImageInline(admin.TabularInline):
    model = PropertyImage
    extra = 1


@admin.register(History)
class HistoryAdmin(admin.ModelAdmin):
    # save_on_top = True
    # list_display = ('title', 'content', 'image', 'is_active', 'created_at', 'slug', 'seo_title', 'seo_keywords', 'seo_description')
    fields = ('title', 'content', 'is_active', 'date_at', 'image', 'position', 'sort', 'descr_for_main', 'short_descr_for_main',  'slug', 'is_main', 'seo_title', 'seo_keywords', 'seo_description')
    prepopulated_fields = {'slug': ('title',)}
    menu_title = "Истории патриотизма"
    menu_group = "Истории патриотизма"
    # readonly_fields = ('created_at',)
    inlines = [PropertyImageInline, ]
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '134'})},
    }


class PropertyImageInlineEvents(admin.TabularInline):
    model = PropertyImageEvents
    extra = 1

class PropertyImageInlineEventsOrg(admin.TabularInline):
    model = PropertyImageEventsOrg
    extra = 1


@admin.register(Events)
class EventsAdmin(admin.ModelAdmin):
    # save_on_top = True
    # list_display = ('title', 'content', 'image', 'is_active', 'created_at', 'slug', 'seo_title', 'seo_keywords', 'seo_description')
    fields = ('title', 'content', 'is_active', 'date_at', 'image', 'short_descr', 'contacts', 'is_moderate', 'slug', 'seo_title', 'seo_keywords', 'seo_description')
    prepopulated_fields = {'slug': ('title',)}
    menu_title = "События"
    menu_group = "События"
    # readonly_fields = ('created_at',)
    inlines = [PropertyImageInlineEvents, PropertyImageInlineEventsOrg]
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '134'})},
    }




class PropertyImageInlineOrgs(admin.TabularInline):
    model = PropertyImageOrgs
    extra = 1

class PropertyImageInlineOrgsOrg(admin.TabularInline):
    model = PropertyImageOrgsOrg
    extra = 1


@admin.register(Orgs)
class OrgsAdmin(admin.ModelAdmin):
    # save_on_top = True
    # list_display = ('title', 'content', 'image', 'is_active', 'created_at', 'slug', 'seo_title', 'seo_keywords', 'seo_description')
    fields = ('title', 'content', 'is_active', 'image', 'fio', 'email', 'phone', 'is_moderate',  'slug', 'seo_title', 'seo_keywords', 'seo_description')
    prepopulated_fields = {'slug': ('title',)}
    menu_title = "Организация"
    menu_group = "Организации"
    inlines = [PropertyImageInlineOrgs, PropertyImageInlineOrgsOrg]
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '134'})},
    }





from django import forms
class SocialModelForm( forms.ModelForm ):
    short_descr = forms.CharField( widget=forms.Textarea, label="Краткое описание")
    class Meta:
        model = SocialInfo
        fields = '__all__'



@admin.register(SocialInfo)
class SocialInfoAdmin(admin.ModelAdmin):
    # save_on_top = True
    # list_display = ('title', 'content', 'image', 'is_active', 'created_at', 'slug', 'seo_title', 'seo_keywords', 'seo_description')
    # fields = ('title', 'content', 'is_active', 'date_at', 'image', 'slug', 'seo_title', 'seo_keywords', 'seo_description')
    fields = ('title', 'image', 'short_descr',  'content', 'is_active', 'social', 'date_at', 'slug', 'seo_title', 'seo_keywords', 'seo_description')

    prepopulated_fields = {'slug': ('title',)}
    menu_title = "Новости с соц. сетей"
    menu_group = "Новости с соц. сетей"
    form = SocialModelForm
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '134'})},
    }


@admin.register(Comments)
class CommentsAdmin(admin.ModelAdmin):
    # save_on_top = True
    # list_display = ('title', 'content', 'image', 'is_active', 'created_at', 'slug', 'seo_title', 'seo_keywords', 'seo_description')
    fields = ('comments_new', 'author', 'comments_text', 'is_moderate', 'likes', 'dislikes')
    readonly_fields = ('comments_date',)
    # prepopulated_fields = {'slug': ('title',)}
    menu_title = "Комментарии"
    menu_group = "Комментарии"
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '134'})},
    }


@admin.register(Photo_Video)
class Photo_Video(admin.ModelAdmin):
    # save_on_top = True
    # list_display = ('title', 'content', 'image', 'is_active', 'created_at', 'slug', 'seo_title', 'seo_keywords', 'seo_description')
    # fields = ('title', 'content', 'is_active', 'date_at', 'image', 'slug', 'seo_title', 'seo_keywords', 'seo_description')
    fields = ('title', 'image', 'is_active', 'link', 'date_at')
    # prepopulated_fields = {'slug': ('title',)}
    menu_title = "Фото и видео"
    menu_group = "Фото и видео"
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '134'})},
    }

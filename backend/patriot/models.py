from django.db import models
from core.base_page.models import BasePage
from filebrowser.fields import FileBrowseField
from django.utils import timezone
from ckeditor_uploader.fields import RichTextUploadingField
import os

from django.conf import settings

from app.settings import MEDIA_ROOT


class News(BasePage):
    """
    Новость.
    """
    image = FileBrowseField("Изображение", max_length=500, extensions=[".jpg", ".png", ".gif"], blank=True, null=True)
    seo_alt = models.CharField(max_length=255, verbose_name='SEO alt', blank=True, null=True)
    # seo_title = models.CharField(max_length=255, verbose_name='SEO title', blank=True, null=True)
    date_at = models.DateTimeField(default=timezone.now, verbose_name='Дата и время новости')

    prepopulated_fields = {'slug': ('title',)}

    sort = models.IntegerField(default=100, verbose_name="Сортировка")
    # is_main = models.BooleanField(default=False, verbose_name="На главной")


    def image_tag(self):
        return '<div class="preview"><a href="/media/%s"><img src="/media/%s" height="100" /></a></div>' % (
        self.image, self.image)

    image_tag.short_description = 'Миниатюра'
    image_tag.allow_tags = True

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
        ordering = ('-date_at',)

    def get_absolute_url(self):
        # todo находим страницу, к которой привязаны новости и цепляем урл от нее
        from core.page.models import Page
        page = Page.objects.filter(page_type=1).first()
        return "{}{}/".format(page.get_absolute_url(), self.slug)

    get_absolute_url.short_description = 'URL'

    def get_parent_absolute_url(self):
        from core.page.models import Page
        page = Page.objects.filter(page_type=1).first()
        return "{}".format(page.get_absolute_url())


class History(BasePage):
    """
    Новость.
    """

    image = FileBrowseField("Картинка для главной", max_length=500, extensions=[".jpg", ".png", ".gif"], blank=True, null=True)

    position = models.CharField(max_length=255, verbose_name="Должность(для главной)", blank=True, default='',)

    descr_for_main = RichTextUploadingField(verbose_name='Описание для главной', blank=True, default='', config_name='awesome_ckeditor')
    short_descr_for_main = RichTextUploadingField(verbose_name='Описание для главной(короткое)', blank=True, default='', config_name='awesome_ckeditor')

    is_main = models.BooleanField(default=False, verbose_name='На главной')

    seo_alt = models.CharField(max_length=255, verbose_name='SEO alt', blank=True, null=True)
    # seo_title = models.CharField(max_length=255, verbose_name='SEO title', blank=True, null=True)
    date_at = models.DateTimeField(default=timezone.now, verbose_name='Дата и время')

    prepopulated_fields = {'slug': ('title',)}

    sort = models.IntegerField(default=100, verbose_name="Сортировка")
    # is_main = models.BooleanField(default=False, verbose_name="На главной")


    def image_tag(self):
        return '<div class="preview"><a href="/media/%s"><img src="/media/%s" height="100" /></a></div>' % (
        self.image, self.image)

    image_tag.short_description = 'Миниатюра'
    image_tag.allow_tags = True

    class Meta:
        verbose_name = 'История'
        verbose_name_plural = 'Истории'
        ordering = ('-date_at',)

    def get_absolute_url(self):
        # todo находим страницу, к которой привязаны новости и цепляем урл от нее
        from core.page.models import Page
        page = Page.objects.filter(page_type=2).first()
        return "{}{}/".format(page.get_absolute_url(), self.slug)

    get_absolute_url.short_description = 'URL'

    def get_parent_absolute_url(self):
        from core.page.models import Page
        page = Page.objects.filter(page_type=2).first()
        return "{}".format(page.get_absolute_url())


class PropertyImage(models.Model):
    property = models.ForeignKey(History, related_name='images', verbose_name="Картинки для историй")
    image = FileBrowseField("Изображение", max_length=500, extensions=[".jpg", ".png", ".gif"], blank=True, null=True)
    sort = models.IntegerField(default=100, verbose_name="Сортировка")

    class Meta:
        verbose_name = 'Картинка'
        verbose_name_plural = 'Картинки для истории'

import uuid

def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    # return 'uploads/%s/%s/%s.jpg' % (filename[:1], filename[2:3], filename) #.jpg.jpg
    return 'uploads/%s' % (filename)



class Events(BasePage):

    image = FileBrowseField("Основная картинка", max_length=500, extensions=[".jpg", ".png", ".gif"], blank=True,
                            null=True)

    short_descr = RichTextUploadingField(verbose_name='Краткое описание', blank=True, default='',
                                                  config_name='awesome_ckeditor')

    contacts = RichTextUploadingField(verbose_name='Контакты', blank=True, default='',
                                         config_name='awesome_ckeditor')

    date_at = models.DateTimeField(default=timezone.now, verbose_name='Дата и время события')


    is_moderate = models.BooleanField(default=False, verbose_name='Одобрено модератором')

    seo_alt = models.CharField(max_length=255, verbose_name='SEO alt', blank=True, null=True)
    # seo_title = models.CharField(max_length=255, verbose_name='SEO title', blank=True, null=True)


    prepopulated_fields = {'slug': ('title',)}

    sort = models.IntegerField(default=100, verbose_name="Сортировка")
    # is_main = models.BooleanField(default=False, verbose_name="На главной")


    def image_tag(self):
        return '<div class="preview"><a href="/media/%s"><img src="/media/%s" height="100" /></a></div>' % (
        self.image, self.image)

    image_tag.short_description = 'Миниатюра'
    image_tag.allow_tags = True

    class Meta:
        verbose_name = 'Мероприятия'
        verbose_name_plural = 'Мероприятия'
        ordering = ('-date_at',)

    def get_absolute_url(self):
        # todo находим страницу, к которой привязаны новости и цепляем урл от нее
        from core.page.models import Page
        page = Page.objects.filter(page_type=3).first()
        return "{}{}/".format(page.get_absolute_url(), self.slug)

    get_absolute_url.short_description = 'URL'

    def get_parent_absolute_url(self):
        from core.page.models import Page
        page = Page.objects.filter(page_type=3).first()
        return "{}".format(page.get_absolute_url())




class PropertyImageEvents(models.Model):
    property = models.ForeignKey(Events, related_name='images_events', verbose_name="Картинки для cобытий")
    # image = FileBrowseField("Изображение", max_length=500, extensions=[".jpg", ".png", ".gif"], blank=True, null=True)
    image = models.ImageField(upload_to=get_file_path, blank=True, null=True, verbose_name="Изображение")
    sort = models.IntegerField(default=100, verbose_name="Сортировка")



    class Meta:
        verbose_name = 'Картинка'
        verbose_name_plural = 'Картинки для событий'

class PropertyImageEventsOrg(models.Model):
    property = models.ForeignKey(Events, related_name='images_events_org', verbose_name="Картинки для Организаций")
    # image = FileBrowseField("Изображение", max_length=500, extensions=[".jpg", ".png", ".gif"], blank=True, null=True)
    image = models.ImageField(upload_to=get_file_path, blank=True, null=True, verbose_name="Изображение")
    sort = models.IntegerField(default=100, verbose_name="Сортировка")

    class Meta:
        verbose_name = 'Картинка'
        verbose_name_plural = 'Картинки для организаций'



class Orgs(BasePage):

    image = FileBrowseField("Основная картинка", max_length=500, extensions=[".jpg", ".png", ".gif"], blank=True,
                            null=True)
                            # null=True, default=os.path.join(settings.BASE_DIR, '..', 'public', 'media') + '/logo-bottom.png')

    fio = models.CharField(max_length=255, verbose_name='ФИО', blank=True, null=True)
    email = models.EmailField(max_length=255, verbose_name='Email', blank=True, null=True)

    phone = models.CharField(max_length=255, verbose_name='Телефон', blank=True, null=True)

    is_moderate = models.BooleanField(default=False, verbose_name='Одобрено модератором')

    seo_alt = models.CharField(max_length=255, verbose_name='SEO alt', blank=True, null=True)
    # seo_title = models.CharField(max_length=255, verbose_name='SEO title', blank=True, null=True)


    prepopulated_fields = {'slug': ('title',)}

    sort = models.IntegerField(default=100, verbose_name="Сортировка")
    # is_main = models.BooleanField(default=False, verbose_name="На главной")


    def image_tag(self):
        return '<div class="preview"><a href="/media/%s"><img src="/media/%s" height="100" /></a></div>' % (
        self.image, self.image)

    image_tag.short_description = 'Миниатюра'
    image_tag.allow_tags = True

    class Meta:
        verbose_name = 'Организации'
        verbose_name_plural = 'Организации'

    def get_absolute_url(self):
        # todo находим страницу, к которой привязаны новости и цепляем урл от нее
        from core.page.models import Page
        page = Page.objects.filter(page_type=4).first()
        return "{}{}/".format(page.get_absolute_url(), self.slug)

    get_absolute_url.short_description = 'URL'

    def get_parent_absolute_url(self):
        from core.page.models import Page
        page = Page.objects.filter(page_type=4).first()
        return "{}".format(page.get_absolute_url())




class PropertyImageOrgs(models.Model):
    property = models.ForeignKey(Orgs, related_name='images_orgs', verbose_name="Картинки для мероприятий организаций")
    # image = FileBrowseField("Изображение", max_length=500, extensions=[".jpg", ".png", ".gif"], blank=True, null=True)
    image = models.ImageField(upload_to=get_file_path, blank=True, null=True, verbose_name="Изображение")
    sort = models.IntegerField(default=100, verbose_name="Сортировка")



    class Meta:
        verbose_name = 'Картинка'
        verbose_name_plural = 'Картинки для событий'

class PropertyImageOrgsOrg(models.Model):
    property = models.ForeignKey(Orgs, related_name='images_orgs_org', verbose_name="Картинки для организаций")
    # image = FileBrowseField("Изображение", max_length=500, extensions=[".jpg", ".png", ".gif"], blank=True, null=True)
    image = models.ImageField(upload_to=get_file_path, blank=True, null=True, verbose_name="Изображение")
    sort = models.IntegerField(default=100, verbose_name="Сортировка")

    class Meta:
        verbose_name = 'Картинка'
        verbose_name_plural = 'Картинки для организаций'



FB = 'FB'
VK = 'VK'
TW = 'TW'
OK = 'OK'
INST = 'INST'

SOCIAL_CHOICES = (
    (FB, 'Facebook'),
    (VK, 'В контакте'),
    (TW, 'Твиттер'),
    (OK, 'Одноклассники'),
    (INST, 'Instagram'),
)


class SocialInfo(BasePage):

    image = FileBrowseField("Основная картинка", max_length=500, extensions=[".jpg", ".png", ".gif"], blank=True,
                            null=True)

    short_descr = models.CharField(max_length=200, verbose_name='Краткий анонс', blank=True, null=True)

    is_moderate = models.BooleanField(default=False, verbose_name='Одобрено модератором')

    date_at = models.DateTimeField(default=timezone.now, verbose_name='Дата и время события')

    social = models.CharField(
        max_length=5,
        choices=SOCIAL_CHOICES,
        default = 'VK',
        verbose_name='Социальная сеть'
    )

    link = models.URLField(max_length=255, verbose_name="Ссылка")

    seo_alt = models.CharField(max_length=255, verbose_name='SEO alt', blank=True, null=True)
    # seo_title = models.CharField(max_length=255, verbose_name='SEO title', blank=True, null=True)


    prepopulated_fields = {'slug': ('title',)}

    sort = models.IntegerField(default=100, verbose_name="Сортировка")
    # is_main = models.BooleanField(default=False, verbose_name="На главной")


    def image_tag(self):
        return '<div class="preview"><a href="/media/%s"><img src="/media/%s" height="100" /></a></div>' % (
        self.image, self.image)

    image_tag.short_description = 'Миниатюра'
    image_tag.allow_tags = True

    class Meta:
        verbose_name = 'Новость из соц. сетей'
        verbose_name_plural = 'Новости из соц. сетей'

    def get_absolute_url(self):
        # todo находим страницу, к которой привязаны новости и цепляем урл от нее
        from core.page.models import Page
        page = Page.objects.filter(page_type=5).first()
        return "{}{}/".format(page.get_absolute_url(), self.slug)

    get_absolute_url.short_description = 'URL'

    def get_parent_absolute_url(self):
        from core.page.models import Page
        page = Page.objects.filter(page_type=5).first()
        return "{}".format(page.get_absolute_url())


class Comments(models.Model):
    comments_text = models.TextField(verbose_name="Текст комментария")
    comments_new = models.ForeignKey(News, verbose_name="Новость")
    comments_date = models.DateTimeField(auto_now_add=True, verbose_name="Дата комментария")
    author = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name="Автор комментария")
    is_moderate = models.BooleanField(default=False, verbose_name='Одобрено модератором')

    likes = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='comment_likes', blank=True, verbose_name='Лайки')
    dislikes = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='comment_dislikes', blank=True, verbose_name='Дизлайки')

    def __str__(self):
        return self.comments_new.title

    class Meta:
         verbose_name_plural = "Комментарии"



class Photo_Video(BasePage):

    image = FileBrowseField("Картинка для фото", max_length=500, extensions=[".jpg", ".png", ".gif"], blank=True,
                            null=True)

    date_at = models.DateTimeField(default=timezone.now, verbose_name='Дата и время')

    link = models.URLField(max_length=255, verbose_name="Ссылка на Youtube для видео", blank=True, null=True)

    seo_alt = models.CharField(max_length=255, verbose_name='SEO alt', blank=True, null=True)
    # seo_title = models.CharField(max_length=255, verbose_name='SEO title', blank=True, null=True)


    prepopulated_fields = {'slug': ('title',)}

    sort = models.IntegerField(default=100, verbose_name="Сортировка")
    # is_main = models.BooleanField(default=False, verbose_name="На главной")


    def image_tag(self):
        return '<div class="preview"><a href="/media/%s"><img src="/media/%s" height="100" /></a></div>' % (
        self.image, self.image)

    image_tag.short_description = 'Миниатюра'
    image_tag.allow_tags = True

    class Meta:
        verbose_name = 'Фото и видео'
        verbose_name_plural = 'Фото и видео'

    def get_absolute_url(self):
        # todo находим страницу, к которой привязаны новости и цепляем урл от нее
        from core.page.models import Page
        page = Page.objects.filter(page_type=5).first()
        return "{}{}/".format(page.get_absolute_url(), self.slug)

    get_absolute_url.short_description = 'URL'

    def get_parent_absolute_url(self):
        from core.page.models import Page
        page = Page.objects.filter(page_type=5).first()
        return "{}".format(page.get_absolute_url())
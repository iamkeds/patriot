from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from datetime import datetime
from patriot.models import News, History, PropertyImage, Events, PropertyImageEvents, PropertyImageEventsOrg, Orgs, PropertyImageOrgs, PropertyImageOrgsOrg, SocialInfo, Comments, Photo_Video

from patriot.forms import AddEventForm

from core.page.models import Page


def home(request):
    c = {}

    c['test'] = 'test field value'

    # news_1 = News.objects.filter(is_active=True)[0:3]
    news_1 = News.objects.filter(is_active=True).filter(date_at__lte=datetime.now()).order_by('sort', '-date_at')[0:3]
    news_2 = News.objects.filter(is_active=True).filter(date_at__lte=datetime.now()).order_by('sort', '-date_at')[3:6]

    # print(news_1)

    c['news_1'] = news_1
    c['news_2'] = news_2

    c['history'] = History.objects.filter(is_main=True).filter(date_at__lte=datetime.now())[0:1]
    c['histories_slide'] = History.objects.filter(is_main=True).filter(date_at__lte=datetime.now()).order_by('sort', '-created_at')[0:4]
    c['history1'] = History.objects.filter(is_active=True).filter(date_at__lte=datetime.now()).order_by('sort', '-created_at')[0:1]

    events_1 = Events.objects.filter(is_active=True).filter(is_moderate=True).filter(date_at__lte=datetime.now()).order_by('sort', '-date_at')[0:4]
    events_2 = Events.objects.filter(is_active=True).filter(is_moderate=True).filter(date_at__lte=datetime.now()).order_by('sort', '-date_at')[4:8]

    c['events1'] = events_1
    c['events2'] = events_2

    # print('events_1')
    #
    # print(events_1)
    #
    # print(c['history'])

    # orgs = Orgs.objects.filter(is_active=True).filter(is_moderate=True).order_by('sort')
    orgs_1 = Orgs.objects.filter(is_active=True).filter(is_moderate=True).order_by('sort')[0:4]
    orgs_2 = Orgs.objects.filter(is_active=True).filter(is_moderate=True).order_by('sort')[4:8]

    c['orgs1'] = orgs_1
    c['orgs2'] = orgs_2

    # print(orgs)

    # c['orgs'] = orgs

    socials = SocialInfo.objects.filter(is_active=True).filter(date_at__lte=datetime.now()).order_by('sort', '-date_at')[0:4]

    c['socials'] = socials

    photo_videos = Photo_Video.objects.filter(is_active=True).filter(date_at__lte=datetime.now()).order_by('sort', '-date_at')[0:7]

    c['photo_videos'] = photo_videos


    # return {
    #     'is_home_page': True
    # }

    return c

    # return render(request, 'home.html', c)

def news(request):
    c = {}

    news = News.objects.filter(is_active=True).filter(date_at__lte=datetime.now()).order_by('sort', '-date_at')

    if request.GET.get('page'):
        page = request.GET.get('page')

    else:
        page = 1

    pag = get_pag(request, 9, 2, 2, page, news, 'news')
    c.update(pag)



    # c['news'] = news

    print(c)


    return c


from django.views.generic.detail import DetailView
# from news.models import News
# from page.models import Page
from django.http import Http404


class NewsView(DetailView):
    model = News
    template_name = 'pages/news_detail.html'

    # context_object_name = 'Test'

    # c['test'] = 'test field value'

    # print('in_news_view')


    def get_context_data(self, **kwargs):
        context = super(NewsView, self).get_context_data(**kwargs)
        # pretty_json = "So pretty so pretty"
        slugs = self.kwargs['url'].split('/')
        context['extra_news'] = News.objects.filter(is_active=True).filter(date_at__lte=datetime.now()).order_by('sort', '-date_at').exclude(is_active=True, slug=slugs[-1])[0:3]
        slugs = self.kwargs['url'].split('/')
        context['news_comments'] = Comments.objects.filter(comments_new=News.objects.filter(is_active=True, slug=slugs[-1]).filter(date_at__lte=datetime.now()).first()).order_by('-comments_date')
        # print('news_comments')
        # print(context['news_comments'])
        return context


    def get_object(self, queryset=None):
        try:
            if 'url' in self.kwargs:
                slugs = self.kwargs['url'].split('/')
                page = Page.objects.filter(is_active=True, page_type=1, slug=slugs[-2]).first()
                # print(page)
                if page:
                    print(News.objects.filter(is_active=True, slug=slugs[-1]).first())
                    return News.objects.filter(is_active=True, slug=slugs[-1]).filter(date_at__lte=datetime.now()).first()
        except IndexError:
            pass
        raise Http404



def history(request):
    c = {}

    c['test'] = 'test field value'

    return c


class HistoryView(DetailView):
    model = History
    template_name = 'pages/history_detail.html'

    # context_object_name = 'Test'

    # c['test'] = 'test field value'

    # print('in_news_view')


    def get_context_data(self, **kwargs):
        context = super(HistoryView, self).get_context_data(**kwargs)

        try:
            if 'url' in self.kwargs:
                slugs = self.kwargs['url'].split('/')
                page = Page.objects.filter(is_active=True, page_type=2, slug=slugs[-2]).first()
                # print(page)
                if page:
                    # print(History.objects.filter(is_active=True, slug=slugs[-1]).first())
                    now_hist = History.objects.filter(is_active=True, slug=slugs[-1]).filter(date_at__lte=datetime.now()).first()
                    all_hist = History.objects.filter(is_active=True).filter(date_at__lte=datetime.now()).order_by('sort', '-created_at')

                    # all_hist = list(all_hist)

                    print (all_hist)

                    context['all_hist'] = all_hist

                    order = 1

                    for hist in all_hist:
                        if hist == now_hist:
                            break

                        order +=1

                    print(order)

                    # total_order = 1
                    #
                    # for hist in all_hist:
                    #     hist.order = total_order
                    #     total_order += 1


                    # print('hhist')
                    # print(now_hist.pk)
                    property = PropertyImage.objects.filter(property__pk=now_hist.pk).order_by('sort')
                    context['images'] = property
                    # print(property[0].image)
                    # print(property[1].image)
                    # print(property[2].image)

                    pag = get_pag_no_req(1, 2, 2, order, all_hist, 'history')
                    context.update(pag)

                    print(context)


                    return context
        except IndexError:
            pass
        raise Http404

        # property = Property.objects.get(pk=1)
        # image_list = property.images.all()

        # pretty_json = "So pretty so pretty"
        # context['extra_news'] = News.objects.filter(is_active=True).order_by('sort', '-date_at')[0:3]
        # context['hello'] = "Hellow hellow"



    def get_object(self, queryset=None):
        try:
            if 'url' in self.kwargs:
                slugs = self.kwargs['url'].split('/')
                page = Page.objects.filter(is_active=True, page_type=2, slug=slugs[-2]).first()
                # print(page)
                if page:
                    # print(History.objects.filter(is_active=True, slug=slugs[-1]).first())
                    return History.objects.filter(is_active=True, slug=slugs[-1]).filter(date_at__lte=datetime.now()).first()
        except IndexError:
            pass
        raise Http404


import math
def get_pag(request, in_page, r, l, page_num, obj_list, prefix):
    page_num = int(page_num)
    extend_pag = '?' + request.META['QUERY_STRING']
    pag_from = (int(page_num) - 1) * in_page
    pag_to = (int(page_num) - 1) * in_page + in_page


    max_p = math.ceil(len(obj_list) / in_page)

    abs_max = math.ceil(len(obj_list) / in_page)
    abs_min = 1

    if page_num < max_p:
        next = page_num + 1
    else:
        next = None

    if page_num > 1:
        prev = page_num - 1
    else:
        prev = None

    dr = r - min([max_p - page_num, r])  # 0
    dl = l - min([page_num - 1, l])  #

    min_p = max([page_num - l - dr, 1])
    max_p = min([page_num + r + dl, max_p])

    # r['from'] = pag_from
    # r['to'] = pag_to

    obj_list = obj_list[pag_from:pag_to]

    r = {}

    r[prefix + '_obj_list'] = obj_list

    r[prefix + '_max'] = abs_max
    r[prefix + '_min'] = abs_min

    r[prefix + '_min_p'] = min_p
    r[prefix + '_max_p'] = max_p
    r[prefix + '_range'] = range(min_p, max_p + 1)
    r[prefix + '_page_num'] = page_num
    r[prefix + '_next'] = next
    r[prefix + '_prev'] = prev
    r[prefix + '_extend_pag'] = extend_pag

    return r

def get_pag_no_req(in_page, r, l, page_num, obj_list, prefix):
        page_num = int(page_num)
        # extend_pag = '?' + request.META['QUERY_STRING']
        pag_from = (int(page_num) - 1) * in_page
        pag_to = (int(page_num) - 1) * in_page + in_page

        max_p = math.ceil(len(obj_list) / in_page)

        abs_max = math.ceil(len(obj_list) / in_page)
        abs_min = 1

        if page_num < max_p:
            next = page_num + 1
        else:
            next = None

        if page_num > 1:
            prev = page_num - 1
        else:
            prev = None

        dr = r - min([max_p - page_num, r])  # 0
        dl = l - min([page_num - 1, l])  #

        min_p = max([page_num - l - dr, 1])
        max_p = min([page_num + r + dl, max_p])

        # r['from'] = pag_from
        # r['to'] = pag_to

        obj_list = obj_list[pag_from:pag_to]

        r = {}

        r[prefix + '_obj_list'] = obj_list

        r[prefix + '_max'] = abs_max
        r[prefix + '_min'] = abs_min

        r[prefix + '_min_p'] = min_p
        r[prefix + '_max_p'] = max_p
        r[prefix + '_range'] = range(min_p, max_p + 1)
        r[prefix + '_page_num'] = page_num
        r[prefix + '_next'] = next
        r[prefix + '_prev'] = prev
        # r[prefix + '_extend_pag'] = extend_pag

        return r

# def get_pag(context, in_page=2, r=2, l=2, page_num=1, list=None, prefix, pag_class = 'mypag'):
#     # request = context['request']
#     if list:
#         page_num = int(page_num)
#         extend_pag = '?' + request.META['QUERY_STRING']
#         pag_from = (int(page_num) - 1) * in_page
#         pag_to = (int(page_num) - 1) * in_page + in_page
#
#
#         max_p = math.ceil(len(list) / in_page)
#
#         if page_num < max_p:
#             next = page_num + 1
#         else:
#             next = None
#
#         if page_num > 1:
#             prev = page_num - 1
#         else:
#             prev = None
#
#         dr = r - min([max_p - page_num, r])  # 0
#         dl = l - min([page_num - 1, l])  #
#
#         min_p = max([page_num - l - dr, 1])
#         max_p = min([page_num + r + dl, max_p])
#
#
#
#
#
#         # r['from'] = pag_from
#         # r['to'] = pag_to
#
#         list = list[pag_from:pag_to]
#
#         r = {}
#
#
#         r['list'] = list
#
#         r['min_p'] = min_p
#         r['max_p'] = max_p
#         r['range'] = range(min_p, max_p + 1)
#         r['page_num'] = page_num
#         r['next'] = next
#         r['prev'] = prev
#         r['extend_pag'] = extend_pag
#         r['pag_class'] = pag_class
#
#
#         return r

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

from filebrowser.base import FileObject
from django.utils.dateformat import DateFormat
import requests
def form_catcher(request):
    c = {}

    # c['test'] = 'test field value'


    if request.method == 'POST':
        # form = AddEventForm(request.POST, request.FILES)
        print('Post...')
        print(request.POST)
        # print(request.FILES['photo'])

        captcha_rs = request.POST.get('g-recaptcha-response')
        url = "https://www.google.com/recaptcha/api/siteverify"
        params = {
            'secret': '6LfsezwUAAAAANn90xMFJyRi0FrwtixaX1S9PiR0',
            'response': captcha_rs,
            'remoteip': get_client_ip(request)
        }
        verify_rs = requests.get(url, params=params, verify=True)
        verify_rs = verify_rs.json()
        st = verify_rs.get("success", False)
        print (st)


        if st:

            form_name = request.POST.get('form_name')

            print(form_name)

            if (form_name == 'events'):
                title = request.POST.get('name')
                short_descr = request.POST.get('comment_short')
                content = request.POST.get('comment')
                contacts = request.POST.get('contacts')
                # date_at = request.POST.get('contacts')

                try:
                    # date_at = datetime.strptime(request.POST['date'] + ' +0300', '%Y-%m-%d %z')
                    date_at = datetime.strptime(request.POST['date'], '%Y-%m-%d')
                except ValueError:
                    print('dateerror')
                # handle this


                q = Events(title=title, short_descr=short_descr, content=content, contacts=contacts, date_at=date_at)
                q.save()

                # instance = ModelWithFileField(file_field=request.FILES['file'])
                # instance.save()

                # im = PropertyImageEvents(property=q, image=request.FILES['photo'], sort=100)
                # im = PropertyImageEvents(property=q, image=request.FILES['photo'])

                for f in request.FILES.getlist('photo'):
                    im = PropertyImageEvents(property=q, image=f)
                    im.save()

                for f in request.FILES.getlist('file'):
                    # im = PropertyImageEventsOrg(property=q, image=FileObject('/media/uploads/05364883-cc53-4a2f-997c-b220d8159ce2.jpg'))
                    im = PropertyImageEventsOrg(property=q, image=f)
                    im.save()

                success = 1





            if (form_name == 'orgs'):
                title = request.POST.get('name')
                fio = request.POST.get('fio')
                email = request.POST.get('email')
                phone = request.POST.get('phone')
                # date_at = request.POST.get('contacts')

                # try:
                #     date_at = datetime.strptime(request.POST['date'], '%Y-%m-%d')
                # except ValueError:
                #     print('dateerror')
                # # handle this


                q = Orgs(title=title, fio=fio, email=email, phone=phone)
                q.save()



                for f in request.FILES.getlist('photo'):
                    im = PropertyImageOrgs(property=q, image=f)
                    im.save()

                for f in request.FILES.getlist('file'):
                    # im = PropertyImageEventsOrg(property=q, image=FileObject('/media/uploads/05364883-cc53-4a2f-997c-b220d8159ce2.jpg'))
                    im = PropertyImageOrgsOrg(property=q, image=f)
                    im.save()



                success = 1

            if (form_name == 'comments'):

                if request.user.is_authenticated():
                    auth = True
                    username = request.user.username
                else:
                    auth = False
                    username = ''
                df = DateFormat(datetime.now())
                date = df.format('d.m.Y')

                text = request.POST.get('text')

                print('text')
                print(text)

                news_id = request.POST.get('news_id')

                print('news_id')
                print(news_id)

                # comments_new = News.objects.get(id=pk)

                if len(text) > 0 and auth:
                    q = Comments(comments_text=text, author=request.user, comments_new=News.objects.get(id=news_id))
                    q.save()

                # text = 'aaa'
                #
                # q = Comments(comments_text=text, author=request.user, comments_product=Mult.objects.get(id=pk))
                # q.save()


                return JsonResponse({'auth': auth, 'username': username, 'date': date, 'text': text})





                success = 1;



    return JsonResponse({'success': success, 'exists': 'aaaa'})

import json
def add_like(request):
    if request.user.is_authenticated():
        auth = True
    else:
        auth = False

    if request.method == 'POST':
        # form = AddEventForm(request.POST, request.FILES)
        print('Post...')
        # print(request.POST)
        json_bin = request.body
        print(request.body)
        try:
            parsed_json = json.loads(json_bin.decode("utf-8"))
        except ValueError:  # includes simplejson.decoder.JSONDecodeError
            # return HttpResponse("Некорректный POST запрос")
            c = {'error': '(Некорректный POST запрос)'}
            return render(request, 'error.html', c)
        print(parsed_json['comm_id'])

        pk = parsed_json['comm_id']

        el = get_object_or_404(Comments, pk=pk)

        print(el)
        if request.user in el.likes.all():
            el.likes.remove(request.user)
            exists = False
            # value = el.likes.all().count()
            # return JsonResponse({'exists': False, 'value': value})
        else:
            el.likes.add(request.user)
            exists = True
            # value = el.likes.all().count()
            # return JsonResponse({'exists': True, 'value': value})
        value = el.likes.all().count()
        return JsonResponse({'auth': auth, 'exists': exists, 'value': value})







    # return JsonResponse({'auth': auth, })

def add_dislike(request):
    if request.user.is_authenticated():
        auth = True
    else:
        auth = False

    if request.method == 'POST':
        # form = AddEventForm(request.POST, request.FILES)
        print('Post...')
        # print(request.POST)
        json_bin = request.body
        print(request.body)
        try:
            parsed_json = json.loads(json_bin.decode("utf-8"))
        except ValueError:  # includes simplejson.decoder.JSONDecodeError
            # return HttpResponse("Некорректный POST запрос")
            c = {'error': '(Некорректный POST запрос)'}
            return render(request, 'error.html', c)
        print(parsed_json['comm_id'])

        pk = parsed_json['comm_id']

        el = get_object_or_404(Comments, pk=pk)

        print(el)
        if request.user in el.dislikes.all():
            el.dislikes.remove(request.user)
            exists = False
            # value = el.likes.all().count()
            # return JsonResponse({'exists': False, 'value': value})
        else:
            el.dislikes.add(request.user)
            exists = True
            # value = el.likes.all().count()
            # return JsonResponse({'exists': True, 'value': value})
        value = el.dislikes.all().count()
        return JsonResponse({'auth': auth, 'exists': exists, 'value': value})


def events(request):
    c = {}

    events = Events.objects.filter(is_active=True).filter(is_moderate=True).filter(date_at__lte=datetime.now()).order_by('sort', '-date_at')

    if request.GET.get('page'):
        page = request.GET.get('page')

    else:
        page = 1

    pag = get_pag(request, 12, 2, 2, page, events, 'events')
    c.update(pag)

    return c


class EventsView(DetailView):
    model = Events
    template_name = 'pages/event_detail.html'

    # context_object_name = 'Test'

    # c['test'] = 'test field value'

    # print('in_news_view')


    # def get_context_data(self, **kwargs):
    #     context = super(NewsView, self).get_context_data(**kwargs)
    #     # pretty_json = "So pretty so pretty"
    #     context['extra_news'] = News.objects.filter(is_active=True).order_by('sort', '-date_at')[0:3]
    #     # context['hello'] = "Hellow hellow"
    #     return context


    def get_object(self, queryset=None):
        try:
            if 'url' in self.kwargs:
                slugs = self.kwargs['url'].split('/')
                page = Page.objects.filter(is_active=True, page_type=3, slug=slugs[-2]).first()
                # print(page)
                if page:
                    print(Events.objects.filter(is_active=True, slug=slugs[-1]).filter(date_at__lte=datetime.now()).first())
                    return Events.objects.filter(is_active=True, slug=slugs[-1]).filter(date_at__lte=datetime.now()).first()
        except IndexError:
            pass
        raise Http404

def orgs(request):

    c = {}

    orgs = Orgs.objects.filter(is_active=True).filter(is_moderate=True).order_by('sort')

    if request.GET.get('page'):
        page = request.GET.get('page')

    else:
        page = 1

    pag = get_pag(request, 12, 2, 2, page, orgs, 'orgs')
    c.update(pag)

    return c




class OrgsView(DetailView):
    model = Orgs
    template_name = 'pages/org_detail.html'

    # context_object_name = 'Test'

    # c['test'] = 'test field value'

    # print('in_news_view')


    # def get_context_data(self, **kwargs):
    #     context = super(NewsView, self).get_context_data(**kwargs)
    #     # pretty_json = "So pretty so pretty"
    #     context['extra_news'] = News.objects.filter(is_active=True).order_by('sort', '-date_at')[0:3]
    #     # context['hello'] = "Hellow hellow"
    #     return context


    def get_object(self, queryset=None):
        try:
            if 'url' in self.kwargs:
                slugs = self.kwargs['url'].split('/')
                page = Page.objects.filter(is_active=True, page_type=4, slug=slugs[-2]).first()
                # print(page)
                if page:
                    print(Orgs.objects.filter(is_active=True, slug=slugs[-1]).first())
                    return Orgs.objects.filter(is_active=True, slug=slugs[-1]).first()
        except IndexError:
            pass
        raise Http404


def socail_info(request):
    c = {}

    socials = SocialInfo.objects.filter(is_active=True).filter(date_at__lte=datetime.now()).order_by('sort', '-date_at')

    if request.GET.get('page'):
        page = request.GET.get('page')

    else:
        page = 1

    pag = get_pag(request, 12, 2, 2, page, socials, 'socials')
    c.update(pag)


    return c


def photo_video(request):
    c = {}

    photo_videos = Photo_Video.objects.filter(is_active=True).filter(date_at__lte=datetime.now()).order_by('sort', '-date_at')

    print(photo_videos)


    if request.GET.get('page'):
        page = request.GET.get('page')

    else:
        page = 1

    pag = get_pag(request, 12, 2, 2, page, photo_videos, 'photo_videos')
    c.update(pag)


    return c


class SocialView(DetailView):
    model = SocialInfo
    template_name = 'pages/social_detail.html'


    def get_object(self, queryset=None):
        try:
            if 'url' in self.kwargs:
                slugs = self.kwargs['url'].split('/')
                page = Page.objects.filter(is_active=True, page_type=5, slug=slugs[-2]).first()
                # print(page)
                if page:
                    print(SocialInfo.objects.filter(is_active=True, slug=slugs[-1]).filter(date_at__lte=datetime.now()).first())
                    return SocialInfo.objects.filter(is_active=True, slug=slugs[-1]).filter(date_at__lte=datetime.now()).first()
        except IndexError:
            pass
        raise Http404





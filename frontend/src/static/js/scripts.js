var CaptchaCallbackRender = function() {
        grecaptcha.render('RecaptchaField1', {'sitekey' : '6LfsezwUAAAAAKjzlwGPTL1bRep2DllarOuQL4FR'});
        grecaptcha.render('RecaptchaField2', {'sitekey' : '6LfsezwUAAAAAKjzlwGPTL1bRep2DllarOuQL4FR'});
    };

$(function(){

	$('.news .show-more').on('click', function(){
		$(this).hide();
		$('.news article').slideDown('slow');
		$('.news-more').fadeIn('slow');
		return false;
	});

	$('.events .show-more').on('click', function(){
		$(this).hide();
		$('.events article').slideDown('slow');
		$('.events-more').fadeIn('slow');
		return false;
	});

	$('.nav-btn').on('click', function(){
		$(this).toggleClass('active');
		$('.menu').slideToggle();
	});

	$(".menu-home").on("click","a", function (event) {
		headeHeight = $('header').height();
		$(".nav-menu li").removeClass('active');
		$(this).parent().addClass('active');

		var id  = $(this).attr('href');
		console.log($(this).attr('href').indexOf("#"));
		if($(this).attr('href').indexOf("#") == 0) {
			event.preventDefault();
			if ($(id).offset()) {
				var	top = $(id).offset().top - headeHeight;
				$('body,html').animate({scrollTop: top}, 800);
			}
			return false;
		}
	});

	$(".gotop").on("click", function(){
		$('body,html').animate({scrollTop: 0}, 800);
		return false;
	});

	$(window).on('load', function(){
		photoHeight = $('.photo article').height();
		$('.video article').height(photoHeight);
		console.log('video h', photoHeight);

		$('.photo [data-vbtype="video"]').height(photoHeight);
	});

	$(window).on('load', function(){
		photoHeight = $('.photo-video-det article:not(.video)').height();

		console.log('photo-video-det h', photoHeight);

		// $('.video article').height(photoHeight);

		if (photoHeight < 5) {
			photoHeight = 160;
		}

		$('.photo-video-det .video-article').height(photoHeight);
		$('.photo-video-det .ph-detail').height(photoHeight);
	});

	$('.open-popup').on('click', function(ev){
		popup = $(this).attr('popup');
		video = $(this).attr('video');

		$('.popup-'+popup).fadeIn().addClass('active');

		$('.popup-video .video-frame').prop('src', 'https://www.youtube.com/embed/'+video+'?rel=0');

		setTimeout(function(){
			$('.popup-video .video-frame')[0].src += "&autoplay=1";
			ev.preventDefault();
		}, 100);

		return false;
	});
	$('.close-popup').on('click', function(){
		$('.popup').removeClass('active').fadeOut();
		$('.popup-video .video-frame').prop('src', '');
		return false;
	});

	$('.add-comment').on('click', function(){
		$('.comment-form').slideToggle();
		return false;
	});

	$('.add-auth').on('click', function(){
		$('.social_auth').slideToggle();
		return false;
	});



});
if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = function (callback, thisArg) {
        thisArg = thisArg || window;
        for (var i = 0; i < this.length; i++) {
            callback.call(thisArg, this[i], i, this);
        }
    };
}


function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}


document.querySelectorAll('.jsForm').forEach(function(form, i) {

        console.log(form);

        form.addEventListener('submit', function(e) {

            e.preventDefault();

            console.log('go', location.pathname);

            var formData = new FormData(this);

            var parent = this.closest('.jsFormP');

            // console.log(parent.getAttribute('name'));

            if(parent.getAttribute('name')) {
                formData.append('form_name', parent.getAttribute('name'));
            }

            // this.querySelectorAll('textarea').forEach(function(textarea, itext) {
            //
            //     // console.log('textarea', textarea);
            //
            //     formData.append(textarea.getAttribute('name'), textarea.value);
            //
            //
            // })



            // formData.append(formiKey, formiValue);

            // parent.querySelectorAll('[data-jsFormI').forEach(function(formi, iformi) {
            //
            //     let formiKey = formi.getAttribute('name') + '[]';
            //
            //     let formiValue=formi.getAttribute('data-jsFormI');
            //
            //     if (formiValue.toString().length<1) {
            //         formiValue = formi.innerText;
            //     }
            //
            //     formData.append(formiKey, formiValue);
            //
            //     // console.log('ii', formiValue);
            //
            // })
            //


            var request = new XMLHttpRequest();


            // var xhr = new XMLHttpRequest();
            request.onreadystatechange = function() {
                if (request.readyState == XMLHttpRequest.DONE) {
                    console.log(request.responseText);
                    console.log(JSON.parse(request.responseText).success)

                    if (JSON.parse(request.responseText).success) {
                        $('.popup').removeClass('active').fadeOut();
		                $('.popup-video .video-frame').prop('src', '');
                    }

                    if (JSON.parse(request.responseText).auth) {
                        window.location.reload(false);
                    }
                }
            }


            // r.open("POST", "/video/make/", true);
            // r.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));


            request.open("POST", "/api/formcatcher/");
            request.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));

            request.send(formData);
            console.log('gof', formData);

            // for (var [key, value] of formData.entries()) {
            //     console.log(key, value);
            // }
            //
            // $('.myModal').removeClass('active');
            //
            // $('.thanks-modal').addClass('active');


        });

    })


$('.jsGo').on('click', function(){

    // console.log(window.location.pathname);

    if (window.location.pathname != '/') {

        console.log($(this).attr('href'));

        if ($(this).attr('href') == '#news') {
            window.location.href = '/news/';
        }

        if ($(this).attr('href') == '#info') {
            window.location.href = '/';
        }

        if ($(this).attr('href') == '#events') {
            window.location.href = '/events/';
        }

        if ($(this).attr('href') == '#patriot-story') {
            window.location.href = '/#patriot-story';
        }

        if ($(this).attr('href') == '#photo') {
            window.location.href = '/#photo';
        }

        if ($(this).attr('href') == '#feed') {
            window.location.href = '/#feed';
        }

        if ($(this).attr('href') == '#list') {
            window.location.href = '/#list';
        }

    }

});


document.querySelectorAll('.addLike').forEach(function(alike, i) {

    alike.addEventListener('click', function(e) {

        var self = this


        var comm_id = this.getAttribute('data-comment-id');

        var json = JSON.stringify({
          comm_id: comm_id,
          // surname: "Цой"
        });


        var request = new XMLHttpRequest();


        // var xhr = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState == XMLHttpRequest.DONE) {
                console.log(request.responseText);

                // JSON.parse(request.responseText).value

                if (JSON.parse(request.responseText).auth) {

                    self.childNodes[1].textContent = ' ' + JSON.parse(request.responseText).value

                    // window.location.reload(false);
                }

            }
        }


        request.open("POST", "/api/add_like/");
        request.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));

        request.send(json);


    })

})

document.querySelectorAll('.addDisLike').forEach(function(adislike, i) {

    adislike.addEventListener('click', function(e) {

        var self = this


        var comm_id = this.getAttribute('data-comment-id');

        var json = JSON.stringify({
          comm_id: comm_id,
          // surname: "Цой"
        });


        var request = new XMLHttpRequest();


        // var xhr = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState == XMLHttpRequest.DONE) {
                console.log(request.responseText);

                // JSON.parse(request.responseText).value

                if (JSON.parse(request.responseText).auth) {
                    // window.location.reload(false);
                    self.childNodes[1].textContent = ' ' + JSON.parse(request.responseText).value
                }

            }
        }


        request.open("POST", "/api/add_dislike/");
        request.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));

        request.send(json);


    })

})


$(document).ready(function(){
    $('.venobox').venobox({
        titleattr: 'data-title'
    });
});


$(document).ready(function(){
  $('.jsMainSlider').slick({
    slidesToShow: 1,
    dots: true,
    // autoplay: true,
    // autoplaySpeed: 5000,
  });
});



